API_ROOT = 'http://doboj:5000'

function callApi(endpoint) {
    const url = API_ROOT + endpoint;
    console.debug(url);

    return fetch(url, { method: 'GET'})
        .then( response => Promise.resolve(response))
        .catch((error) => {
            return Promise.reject(error);
        });
}

export const pauseTrip = () => callApi('/api/pause');
export const stopTrip = () => callApi('/api/stop');
export const resumeTrip = () => callApi('/api/resume');

export const startTrip = (tripType, odometerInMeters, loggingEnabled) => {
    console.log(tripType);
    console.log(odometerInMeters);
    console.log(loggingEnabled);
    const url = API_ROOT + "/api/start";
    const body = {trip_type: tripType, odometer_in_meters:  odometerInMeters, enable_logging: loggingEnabled};
    console.log(JSON.stringify(body));
    return fetch(url,
        {
            headers: {'Content-Type': 'application/json'},
            method: 'POST',
            body: JSON.stringify(body)
        })
        .then( response => Promise.resolve(response))
        .catch((error) => {
            return Promise.reject(error);
        });

}