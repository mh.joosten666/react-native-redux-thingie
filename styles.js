import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#000000',
      alignItems: 'center',
      justifyContent: 'center',   
      
    },
    labelText: {
      color: '#ffff00',
      fontSize: 20
    },
    
    valueText: {
        color: '#00ff00',
        fontSize: 20
      }
  });

  export default styles
  