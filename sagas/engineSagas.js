import { call, put, takeEvery, takeLatest, all, fork } from 'redux-saga/effects'
import {pauseTrip, resumeTrip, startTrip, stopTrip} from "../api/engineApi";



function* executeRequest(action) {
    const api = selectApi(action.type);
    try {
        let response;
        if (action.type === "TRIP_START") {
          response = yield call(api, action.payload.tripType, action.payload.odometerInMeters, action.payload.loggingEnabled);
        }
        else {
            response = yield call(api);
        }
        console.debug(response.ok);
        if (response.ok) {
            console.log('jeeeej');
        }
        else {
            console.log('boeeeh!!');
            yield put({type: "ENGINE_CALL_FAILED", payload: "oopsie"});
        }

    }
    catch (e) {
        yield put({type: "ENGINE_CALL_FAILED", payload: e.message});
        console.log("erreeeurrr" +e.message);
    }


}


const selectApi = actionType => {
    switch (actionType) {
        case "TRIP_PAUSE":
            return pauseTrip;
        case "TRIP_RESUME":
            return resumeTrip;
        case "TRIP_STOP":
            return stopTrip;
        case "TRIP_START":
            return startTrip;
    }
};

export function* rootSaga () {
    yield all([
        fork(pauseSaga), // saga1 can also yield [ fork(actionOne), fork(actionTwo) ]
        fork(stopSaga),
        fork(resumeSaga),
        fork(startSaga)
    ]);
}
export function* pauseSaga() {
    yield takeLatest("TRIP_PAUSE", executeRequest);
}

export function* stopSaga() {
    yield takeLatest("TRIP_STOP", executeRequest);
}

export function* resumeSaga() {
    yield takeLatest("TRIP_RESUME", executeRequest);
}

export function* startSaga() {
    yield takeLatest("TRIP_START", executeRequest);

}



//export default pauseSaga;