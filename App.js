import React from 'react';
import {Provider} from "react-redux";
import MainView from "./components/mainView";
import {initStore} from "./store";


export default class App extends React.Component {


  render() {
    const store = initStore();
    return (
      
     <Provider store={store}>
      <MainView></MainView>            
      </Provider>
     
    
    );
  }
}

