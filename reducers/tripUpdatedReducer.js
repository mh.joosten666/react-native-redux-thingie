const tripUpdatedReducer = (state = {
    tripState: "NO_TRIP_ACTIVE",
    tripType: "N/A",
    odometerStart: 0,
    odometerEnd: 0,
    startedOn: 0,
    duration: 0,
    distance: 0,
    averageSpeed: 0
}, action) => {

    switch (action.type) {
        case 'TRIP_UPDATED':
            //let thetripState = determineTripState(action.payload.event);       
            if (action.payload.event === 'stopped') {
                state = {
                    ...state,
                    tripState: 'NO_TRIP_ACTIVE',
                    odometerEnd: action.payload.data._TripData__odometer_start + action.payload.data._TripData__dist            
                };
            }

            if (action.payload.event === 'starting') {
                state = {
                    ...state,
                    tripState: 'WAITING_TO_START'
                };
            }

            if (action.payload.event === 'started') {
                state = {
                    ...state,
                    tripState: 'TRIP_ACTIVE'
                };
            }
            
            if (action.payload.event === 'paused') {
                state = {
                    ...state,
                    tripState: 'TRIP_PAUSED'
                };
            }

            if (action.payload.event === 'updated') {
                state = {
                    ...state,
                    tripState: 'TRIP_ACTIVE',
                    tripType: action.payload.data._TripData__type == 0 ? "BUSINESS" : "NON_BUSINESS" ,
                    odometerStart: action.payload.data._TripData__odometer_start,
                    duration: action.payload.data._TripData__duration,
                    distance: action.payload.data._TripData__dist,
                    averageSpeed: action.payload.data._TripData__avg_speed
                };
            }
            console.log(state);
            break;

    }


    return state;
};


// const determineTripState = (event) => {
//     if (event === "starting" || event === "stopped") {
//         return "NO_TRIP_ACTIVE";
//     }

//     if (event === "paused") {
//         return "TRIP_PAUSED";
//     }

//     return "TRIP_ACTIVE";
// }



export default tripUpdatedReducer;