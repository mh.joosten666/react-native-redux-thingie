const currentDataReducer = (state = {
    time: 0,
    lat: 0,
    lon: 0,
    speed: 0
}, action) => {
    switch (action.type) {
        case "CURRENT_DATA_UPDATE":
            state = {
                ...state,
                time: action.payload.time,
                lat: action.payload.latitude,
                lon: action.payload.longitude,
                speed: action.payload.speed
            };
            break;

    }
    return state;
};

export default currentDataReducer;
