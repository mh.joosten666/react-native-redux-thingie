import { Client, Message} from 'react-native-paho-mqtt';
import { updateCurrentData, tripUpdated } from './actions/currentDataActions'

const reduxMqttMiddleware = mqttUri => ({ dispatch, state }) => {
            // Create a client instance
            const client = new Client({
                uri: mqttUri,
                clientId: 'clientId',
                storage: myStorage
            });
    
            // set event handlers
            client.on('connectionLost', (responseObject) => {
                if (responseObject.errorCode !== 0) {
                    console.log("Kapot!"+responseObject.errorMessage);
                }
            });
            client.on('messageReceived', (message) => {                
                let msg = JSON.parse(message.payloadString);
                if (message.destinationName === "data/throttled/gps") {                    
                    dispatch(updateCurrentData(msg.t,msg.p[0], msg.p[1], msg.v));
                }
                if (message.destinationName === "tripengine") {
                    console.log(msg.event);
                    dispatch(tripUpdated(msg.event, msg.data));
                }
            });
    
            client.connect()
                .then(() => {
                    // Once a connection has been made, make a subscription and send a message.
                    console.log('onConnect');
                    console.log('subscribe');
                    client.subscribe('data/throttled/gps');
                    client.subscribe('tripengine');
                    return;               
                }).catch((responseObject) => {
                    if (responseObject.errorCode !== 0) {
                        console.log('onConnectionLost:' + responseObject.errorMessage);
                    }
                });
    
    
                return next => (action) => {
                    next(action);
                }       
                
    
  };
  

  //Set up an in-memory alternative to global localStorage
const myStorage = {
    setItem: (key, item) => {
        console.log("meh");
        console.log(key + "," + item);
        myStorage[key] = item;
    },
    getItem: (key) => myStorage[key],
    removeItem: (key) => {
        console.log("moeh");
        delete myStorage[key];
    },
};

  export default reduxMqttMiddleware;