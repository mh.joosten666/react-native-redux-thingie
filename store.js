import {createStore, combineReducers, applyMiddleware} from "redux";
import currentDataReducer from './reducers/currentDataReducer';
import tripUpdatedReducer  from './reducers/tripUpdatedReducer';
import reduxMqttMiddleware from './middleware.js';
import createSagaMiddleware from 'redux-saga'
import {pauseSaga, resumeSaga, rootSaga, stopSaga} from "./sagas/engineSagas";



export const initStore = () => {
    const sagaMiddleware = createSagaMiddleware();
    const store = createStore(
    combineReducers({
        currentData: currentDataReducer,
        tripUpdated: tripUpdatedReducer
    }),
    applyMiddleware(sagaMiddleware, reduxMqttMiddleware('ws://doboj:1884/ws')));


    sagaMiddleware.run(rootSaga);

    return store;
};


// export default createStore(
//     const sagaMiddleware = createSagaMiddleware()
//     combineReducers({
//         currentData: currentDataReducer,
//         tripUpdated: tripUpdatedReducer
//     }),
//     {},
//     applyMiddleware(sagaMiddleware, reduxMqttMiddleware('ws://doboj:1884/ws'))
//
// );

