import CurrentData from "../containers/currentData";
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import { Dimensions } from 'react-native';
import React from 'react';
import tripInformation from "../containers/tripInformation";

export default class MainView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          index: 0,
          routes: [
            { key: 'currentData', title: 'Current' },
            { key: 'tripData', title: 'Trip data'}
          ],
        };
    }
    
      render() {
        return (
            <TabView
                navigationState={this.state}
                renderScene={SceneMap({
                currentData: CurrentData,
                tripData: tripInformation
                })}
                onIndexChange={index => this.setState({ index })}
                initialLayout={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height }}
                tabBarPosition='bottom'>
        </TabView>
                
        );
    

    }
}