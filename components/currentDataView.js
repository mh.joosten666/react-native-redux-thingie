import React from 'react';
import { Text, View } from 'react-native';
import styles from '../styles';

export default class CurrentDataView extends React.Component {
    constructor(props) {
        super(props);        
    }
    
    render() {
      return (
        <View>
          <Text style={styles.labelText}>Latitude</Text>
          <Text style={styles.valueText}>{this.props.lat}</Text>
          <Text style={styles.labelText}>Longitude</Text>
          <Text style={styles.valueText}>{this.props.lon}</Text>
          <Text style={styles.labelText}>Speed</Text>
          <Text style={styles.valueText}>{this.props.speed} km/h</Text>          
        </View>
      );
    }
  }