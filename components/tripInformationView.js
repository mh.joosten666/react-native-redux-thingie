import React from 'react';
import { Text, View, Button } from 'react-native';
import styles from '../styles';

export const TripInformationView = (props) => {

      return (
        <View>
          <Text style={styles.labelText}>Trip state</Text>
          <Text style={styles.valueText}>{props.tripState}</Text>
          <Text style={styles.labelText}>Trip type:</Text>
          <Text style={styles.valueText}>{props.tripType}</Text>
          <Text style={styles.labelText}>Odometer start:</Text>
          <Text style={styles.valueText}>{props.odometerStart}</Text>
          <Text style={styles.labelText}>Started on:</Text>
          <Text style={styles.valueText}>{props.startedOn}</Text>
          <Text style={styles.labelText}>Duration:</Text>
          <Text style={styles.valueText}>{props.duration}</Text>
          <Text style={styles.labelText}>Distance:</Text>
          <Text style={styles.valueText}>{props.distance}</Text>
          <Text style={styles.labelText}>Average speed:</Text>
          <Text style={styles.valueText}>{props.averageSpeed}</Text>
            <Button title="Start" onPress={props.onStartButtonClicked}/>
            <Button title="Stop" onPress={props.stopButtonClicked}/>
            <Button title={props.titleForPauseResumeButton} onPress={props.pauseResumeButtonClicked}/>
        </View>
      );

  };