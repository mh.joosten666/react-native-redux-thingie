Test project using react-native, redux (and eventually mqtt) to update an ui (from a mqtt topic)
To start:
- Make sure MQTT broker is running somewhere
- TODO: topic names
- Install Expo app on your mobile phone
- Run `npm start`
- Scan QR code from Expo app
- Application will run

This project was created with create-react-native-app bootstrapper