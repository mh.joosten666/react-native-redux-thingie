export function updateCurrentData(time, lat, lon, speed) {
    return {
        type: "CURRENT_DATA_UPDATE",
        payload: { time: time,
                  latitude: lat,
                  longitude: lon,
                  speed: speed
                 }
    }
}


export function tripUpdated(event, data) {
    return {
        type: "TRIP_UPDATED",
        payload: { event: event,
                   data: data
                 }
    }
}


export function stopTrip() {
    return {
        type: "TRIP_STOP",
        payload: {}

    }
}

export function pauseTrip() {
    return {
        type: "TRIP_PAUSE",
        payload: {}

    }
}

export function resumeTrip() {
    return {
        type: "TRIP_RESUME",
        payload: {}

    }
}


export function startTrip(tripType, odometerInMeters, loggingEnabled) {
    return {
        type: "TRIP_START",
        payload: {
            tripType: tripType,
            odometerInMeters: odometerInMeters,
            loggingEnabled: loggingEnabled
        }

    }
}

export function engineCallSucceeded() {
    return {
        type: "ENGINE_CALL_SUCCEEDED",
        payload: {}

    }
}

export function engineCallFailed(message) {
    return {
        type: "ENGINE_CALL_FAILED",
        payload: message

    }
}

