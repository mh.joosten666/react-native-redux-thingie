import React from 'react';
import {View, Button, Text, TextInput, CheckBox, Picker} from 'react-native';
import { TripInformationView } from '../components/tripInformationView';
import styles from '../styles'
import {connect} from "react-redux";
import {pauseTrip, resumeTrip, startTrip, stopTrip} from "../actions/currentDataActions";
import ConfirmDialog from "react-native-simple-dialogs/src/ConfirmDialog";
import ModalDropdown from "react-native-modal-dropdown/components/ModalDropdown";

class TripInformation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {showDialog: false, selectedType: 0, odometerReading: "", loggingEnabled: false};
    }
    render() {

        return (
        <View style={styles.container}>
            <TripInformationView
                tripState={this.props.tripInfo.tripState}
                tripType={this.props.tripInfo.tripType}
                odometerStart={this.props.tripInfo.odometerStart}
                startedOn={this.props.tripInfo.startedOn}
                duration={this.props.tripInfo.duration}
                distance={this.props.tripInfo.distance}
                averageSpeed={this.props.tripInfo.averageSpeed}
                titleForPauseResumeButton={this.getTitleForPauseResumeButton()}
                pauseResumeButtonClicked={() => {this.pauseResumeButtonClicked()}}
                stopButtonClicked={() => this.props.stopTrip()}
                onStartButtonClicked={() => this.setState({showDialog: true})}>

            </TripInformationView>
            <ConfirmDialog
                title="Start a new trip"
                visible={this.state.showDialog}
                onTouchOutside={() => this.setState({showDialog: false})}
                positiveButton={{
                    title: "START",
                    onPress: () => {
                        this.setState({showDialog: false});
                        this.props.startTrip(Number(this.state.selectedType), this.state.odometerReading * 1000, this.state.loggingEnabled);
                        console.log(this.state)

                    }
                }}
                negativeButton={{
                    title: "CANCEL",
                    disabled: false,
                    titleStyle: {
                        color: 'blue',
                        colorDisabled: 'aqua',
                    },
                    style: {
                        backgroundColor: 'transparent',
                        backgroundColorDisabled: 'transparent',
                    },
                    onPress: () => {
                        this.setState({showDialog: false});
                    }
                }}>

                <View>
                    <Text>Select trip type</Text>
                    <Picker prompt="Select trip type" selectedValue={this.state.selectedType}  onValueChange={(itemValue, itemIndex) => this.setState({selectedType: itemValue})}>
                        <Picker.Item label="Business" value="0" />
                        <Picker.Item label="Non business" value="1" />
                    </Picker>
                    <TextInput value={this.state.odometerReading} underlineColorAndroid = "transparent" placeholder='Enter odometer reading' onChangeText={(text) => {
                        console.log(text);
                        this.setState({odometerReading: text})
                    }
                    }/>
                    <CheckBox value={this.state.loggingEnabled} onValueChange={(value) => this.setState({loggingEnabled: value})} />
                    <Text>
                        Enable logging
                    </Text>


                </View>
            </ConfirmDialog>

        </View>
    );
  }

    getTitleForPauseResumeButton() {
        console.log("props" + this.props);
        if (this.props.tripInfo.tripState === "TRIP_PAUSED") {
            return "Resume";
        }

        if (this.props.tripInfo.tripState !== "WAITING_TO_START") {
            return "Pause";
        }

        return "Don't push me";
    }

    pauseResumeButtonClicked() {
      //console.log(this.props);
      if (this.props.tripInfo.tripState === "TRIP_PAUSED") {
          console.info("resume clicked");
          this.props.resumeTrip();
            }
            else {
          console.info("pause clicked");
          this.props.pauseTrip();
            }
    }

    onStartButtonClicked() {


    }
}


const mapStateToProps = (state) => {
  return {
      tripInfo: state.tripUpdated,
      
  };
};

const mapDispatchToProps = (dispatch) => {
    return {
        stopTrip: () => {
            dispatch(stopTrip());
        },
        pauseTrip: () => {
            dispatch(pauseTrip());
        },
        resumeTrip: () => {
            dispatch(resumeTrip());
        },
        startTrip: (type, odometerInMeters, loggingEnabled) => {
            dispatch(startTrip(type,odometerInMeters,loggingEnabled));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TripInformation);

