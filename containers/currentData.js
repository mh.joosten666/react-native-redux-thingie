import React from 'react';
import { View, Button } from 'react-native';
import CurrentDataView from '../components/currentDataView';
import { updateCurrentData } from '../actions/currentDataActions'
import styles from '../styles'
import {connect} from "react-redux";

class CurrentData extends React.Component {
  render() {
    return (
        <View style={styles.container}>
          <CurrentDataView lat={this.props.currentData.lat} lon={this.props.currentData.lon} speed={this.props.currentData.speed}></CurrentDataView> 
        </View>
    );
  }
}


const mapStateToProps = (state) => {
  return {
      currentData: state.currentData,
      
  };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setCurrentData: (time, lat, lon, speed) => {
            dispatch(updateCurrentData(time, lat, lon, speed));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CurrentData);

